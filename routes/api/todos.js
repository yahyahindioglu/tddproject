const router = require('express').Router();
const Todo = require('../../schema/Todo');

router.post('/', async (req, res, next) => {
    let { title } = req.body;
    await Todo.create({title});
    return res.status(201).send({title:title});
});

router.get('/', async (req, res, next) => {
    var data = await Todo.find();
    res.status(200).send(data);
});

module.exports = router;
