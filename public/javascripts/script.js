getTodos();
$("#submit").click(function(e){
    e.preventDefault();
    let title = $("#todo").val().trim();
    $.ajax({
        url:'/api/todos',
        type:'post',
        data:{title:title},
        success: function(res) {
            if(res){
                $('#list').append('<li>'+res.title+'</li>')
            }else{

            }
        }
    });
});

function getTodos() {
    $.ajax({
        url:'/api/todos',
        type:'get',
        success: function(res) {
            res.forEach(row => {
                $('#list').append('<li>'+row.title+'</li>')
            })
        }
    });
}
