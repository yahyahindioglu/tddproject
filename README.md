#### ToDos Demo

## You can run the application after setup. [Run App](http://localhost:3000)

## 🚀&nbsp; Installation and Documentation

```
git clone https://gitlab.com/yahyahindioglu/tddproject.git

cd <PROJECT-DIR>

npm install

Ren ".env.example" ".env" // Update mongodb url with yours Eg: mongodb://localhost:27017/tdddemo

npm run dev

## 🚀 Run with Docker

Ren ".env.example" ".env"

docker-compose build

docker-compose up

//The docker compose include mongodb container setup so you dont have to set db

## 🚀 Running Tests

npm run test

```

## 📫&nbsp; Have a question? Want to chat? Ran into a problem?

#### *Website [yahyahindioglu.com](http://yahyahindioglu.com)*

#### *My [Portfolio](http://yahyahindioglu.com/#/portfolio)*

#### *[LinkedIn](https://www.linkedin.com/in/yahyahindioglu/) Account*
