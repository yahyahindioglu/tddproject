FROM node:14

WORKDIR /app

COPY package*.json ./
COPY .env .

RUN npm install

# Bundle app source
COPY . .

EXPOSE 3000

CMD [ "npm", "run", "start" ]