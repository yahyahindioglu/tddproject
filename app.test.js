const request = require('supertest');
const app = require('./app');

describe('Todos API', () => {
    it('should get all todos', () => {
        return request(app)
        .get('/api/todos')
        .expect('Content-Type',/json/)
        .expect(200)
        .then((response) => {
            expect(response.body).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                        title: expect.any(String)
                    })
                ])
            )
        })
    });
    it('should create a task', () => {
        return request(app)
        .post('/api/todos')
        .send({
            title: 'Test Task'
        })
        .expect('Content-Type',/json/)
        .expect(201)
        .then((response) => {
            expect(response.body).toEqual(
                expect.objectContaining({
                    title: 'Test Task'
                })
            )
        })
    });
});